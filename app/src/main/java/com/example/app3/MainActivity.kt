import android.annotation.SuppressLint

import android.os.Bundle
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app3.databinding.ActivityMainBinding
import com.example.app3.model.Oil
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.app3.R
import android.widget.Toast
import android.util.Log
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        val view = binding.root
        setContentView(view)

        val itemList: List<Oil> = listOf(
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20", 37.24),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91", 38.08),
            Oil("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95", 38.35),
            Oil("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95", 45.84),
            Oil("เชลล์ ดีเซล B20", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล", 36.34),
            Oil("เชลล์ ฟิวเซฟ ดีเซล B7", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล", 36.34),
            Oil("เชลล์ วี-เพาเวอร์ ดีเซล B7", 47.06)
        )

        val recyclerView = findViewById<RecyclerView>(R.id.RecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ItemAdapter(itemList)
    }

    class ItemAdapter(val items: List<Oil>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

        class ViewHolder(private val itemView : View) : RecyclerView.ViewHolder(itemView){
            val price = itemView.findViewById<TextView>(R.id.Price)
            val name = itemView.findViewById<TextView>(R.id.Name)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val adapterLayout = LayoutInflater.from(parent.context).inflate(R.layout.item, parent,false)
            return ViewHolder(adapterLayout)

        }


        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.name.text = items[position].oil
            holder.price.text = items[position].price.toString()

        }

        override fun getItemCount(): Int {
            TODO("Not yet implemented")
            return items.size
        }

    }

}
