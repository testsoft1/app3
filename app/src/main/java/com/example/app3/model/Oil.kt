package com.example.app3.model
import android.annotation.SuppressLint
import androidx.annotation.StringRes
@SuppressLint("SupportAnnotationUsage")
class Oil (
    @StringRes val oil: String,
    @StringRes val price: Double
)